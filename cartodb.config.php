<?php

function getConfig() {
  $config = array();

  $config['key'] = 'key';
  $config['secret'] = 'secret';
  $config['email'] = 'mail@example.com';
  $config['password'] = 'password';
  $config['subdomain'] = 'subdomain';
  $config['api_key'] = 'api_key';
  return $config;
}
